<?php
// $Id: domain_languages.module,v 1.0 2011/01/14 11:50:58 bpresles Exp $


/**
 * @defgroup domain_languages Domain Languages: Domain language support
 *
 * Allows domain to be associated to a language
 */

/**
 * @file
 * Interface for domain language support
 *
 * @ingroup domain_languages
 */

// Alias id used for a main domain entry
define('DOMAIN_LANGUAGES_MAIN_DOMAIN_ALIAS_ID', - 1);

/**
 * Implement hook_domain_bootstrap_lookup().
 *
 * Add language information to the passed $domain object
 *
 * @param $domain
 * An array containing current domain (host) name and
 * the results of lookup against {domain} table.
 *
 * @see domain_resolve_name()
 *
 * @return
 * An array containing at least 'languages' => array of associated languages codes.
 */
function domain_languages_domain_bootstrap_lookup($domain) {
  $domain_languages = array();
  
  // Add language information to the passed $domain object
  if (isset($domain['active_alias_id'])) {
    $domain_languages['languages'] = domain_languages_get_domain_languages($domain['domain_id'], $domain['active_alias_id']);
  }
  else {
    $domain_languages['languages'] = domain_languages_get_domain_languages($domain['domain_id']);
  }
  
  return $domain_languages;
}

/**
 * Implement hook_enable().
 *
 * Register domain_alias with domain bootstrap so we can use domain_bootstrap hooks.
 */
function domain_languages_enable() {
  domain_bootstrap_register();
}

/**
 * Implement hook_disable().
 */
function domain_languages_disable() {
  domain_bootstrap_unregister('domain_languages');
}

/**
 * Implements hook_form_form_id_alter() for domain form
 */
function domain_languages_form_alter(&$form, &$form_state, $form_id) {
  // Alter domain form or domain configure form
  if ($form_id == 'domain_form' || $form_id == 'domain_configure_form') {  
    $domain_id = NULL;
    
    // If the form is the domain_configure_form, we are editing default domain (id=0)
    if ($form_id == 'domain_configure_form') {
      $domain_id = 0;
    }
    // It's another domain, so get the specific id
    else if (isset($form['domain_id']['#value'])) {
      $domain_id = $form['domain_id']['#value'];
    }
    
    // Don't allow users to set language settings at creation time, as we can't save the information
    if (isset($domain_id)) {
      $languages = language_list();
      $languages_options = array();
      
      foreach ($languages as $key => $language) {
        if ($language->language != 'en') {
          $languages_options[$language->language] = $language->name;
        }
      }
    
      $current_langs_defaults = - 1;
      $current_langs_defaults = domain_languages_get_domain_languages_defaults($domain_id);
      
      $languages_values = array();
      $defaults_values = array();
      
      foreach ($languages_options as $lang_code => $lang_name) {
        if (isset($current_langs_defaults[$lang_code])) {
          $languages_values[] = $current_langs_defaults[$lang_code]['code'];
          $defaults_values[] = ($current_langs_defaults[$lang_code]['default'] == 1 ? $lang_code : 0);
        } 
        else {
          $defaults_values[] = 0;
        }
      }
      
      $form['domain']['languages'] = array(
        '#type' => 'checkboxes', '#title' => t('Domain languages'), '#required' => FALSE, '#options' => $languages_options, '#default_value' => $languages_values, '#description' => t('Select the languages to which assign this domain.') 
      );
      
      $form['domain']['defaults'] = array(
        '#type' => 'checkboxes', '#title' => t('Default domain for these languages'), '#required' => FALSE, '#options' => $languages_options, '#default_value' => $defaults_values, '#description' => t('Whether this domain is default one for the selected languages.') 
      );
      
      $form['#submit'][] = 'domain_languages_domain_form_submit';
      $form['#validate'][] = 'domain_languages_domain_form_validate';
    }
  }
}

/**
 * Submit callback for domain form
 *
 * @param array $form The domain form
 * @param array $form_state The form values
 */
function domain_languages_domain_form_validate($form, &$form_state) {
  $domain_id = NULL;
  
  // If the form is the domain_configure_form, we are editing default domain (id=0) 
  if($form['form_id']['#value'] == 'domain_configure_form') {
    $domain_id = 0;
  }
  // It's another domain, so get the specific id
  elseif (isset($form_state['values']['domain_id'])) {
    $domain_id = $form_state['values']['domain_id'];
  }
  
  if (isset($domain_id)) {  
    // Validating that the use didn't select a language as default that is not assigned to this domain
    foreach ($form_state['values']['defaults'] as $lang_code => $value) {
      // If language has been selected as default
      if (is_string($value) && $lang_code == $value) {
        
        // Does the main domain have the language as default already?
        $default_alias_domain_for_language = domain_languages_get_default_domain_alias_for_language($domain_id, $lang_code);
        
        // If the main domain already is the default domain for this language, raise an error
        if (isset($default_alias_domain_for_language) && $default_alias_domain_for_language['type'] == 'alias' && $default_alias_domain_for_language['details'] != -1) {          
          form_error($form['domain']['defaults'], t("You can't select !domain as default domain for !lang_code language as the !alias_pattern is already the default alias for it",
                    array('!domain' => $form_state['values']['subdomain'], '!lang_code' => $lang_code, '!alias_pattern' => $default_alias_domain_for_language['details']['pattern'])));
          break;
        }
        
        // If language is not part of domain languages
        if(!is_string($form_state['values']['languages'][$lang_code]) || $form_state['values']['languages'][$lang_code] != $lang_code) {
          form_error($form['domain']['defaults'], t("You can't select a language as default if it's not one of the domain language"));
          break;
        }
      }
    }
  }
}

/**
 * Submit callback for domain form
 *
 * @param array $form The domain form
 * @param array $form_state The form values
 */
function domain_languages_domain_form_submit($form, &$form_state) {
  $domain_id = NULL;
  
  // If the form is the domain_configure_form, we are editing default domain (id=0)
  if($form['form_id']['#value'] == 'domain_configure_form') {
    $domain_id = 0;
  }
  // It's another domain, so get the specific id
  elseif (isset($form_state['values']['domain_id'])) {
    $domain_id = $form_state['values']['domain_id'];
  }
  
  if (isset($domain_id)) {  
    // Constructing languages and default informations from submitted values
    $languages_infos = array();
    foreach ($form_state['values']['languages'] as $lang_code => $value) {    
      if (is_string($value) && $value == $lang_code) {    
        $languages_infos[] = array(
          'code' => $lang_code,
          'default' => (is_string($form_state['values']['defaults'][$lang_code]) && $form_state['values']['defaults'][$lang_code] == $lang_code),
        ); 
      }
    }
    // Removing previous information
    domain_languages_delete_languages_for_domain_alias($domain_id, DOMAIN_LANGUAGES_MAIN_DOMAIN_ALIAS_ID);
  
    // Saving updated information
    domain_languages_save_languages($languages_infos, $domain_id, DOMAIN_LANGUAGES_MAIN_DOMAIN_ALIAS_ID);
  }
}


/**
 * Implements hook_form_form_id_alter() for domain alias form
 */
function domain_languages_form_domain_alias_form_alter(&$form, &$form_state) {
  $languages = language_list();
  $languages_options = array();
  $languages_defaults = array();
  
  foreach ($languages as $key => $language) {    
    if ($language->language != 'en') {
      $languages_options[$language->language] = $language->name;
    }
  }
  
  // Adding language checkboxes to existing aliases
  foreach ($form['domain_alias'] as $alias_id => $alias_info) {
    
    $current_langs_defaults = - 1;
    if (isset($form['domain_id']['#value']) && $alias_id > 0) {
      $current_langs_defaults = domain_languages_get_domain_languages_defaults($form['domain_id']['#value'], $alias_id);
    }
    else {
      continue;
    }
    
    $languages_values = array();
    $defaults_values = array();
    
    foreach ($languages_options as $lang_code => $lang_name) {
      if (isset($current_langs_defaults[$lang_code])) {
        $languages_values[] = $current_langs_defaults[$lang_code]['code'];
        $defaults_values[] = ($current_langs_defaults[$lang_code]['default'] == 1 ? $lang_code : 0);
      } 
      else {
        $defaults_values[] = 0;
      }
    }
        
    $form['domain_alias'][$alias_id]['languages'] = array(
      '#type' => 'checkboxes', '#required' => FALSE, '#options' => $languages_options, '#default_value' => $languages_values
    );
    $form['domain_alias'][$alias_id]['defaults'] = array(
      '#type' => 'checkboxes', '#required' => FALSE, '#options' => $languages_options, '#default_value' => $defaults_values
    );
  }
  
  // Adding language checkboxes to new aliases
  foreach ($form['domain_alias_new'] as $index => $alias_info) {
    $form['domain_alias_new'][$index]['languages'] = array(
      '#type' => 'checkboxes', '#required' => FALSE, '#options' => $languages_options 
    );
    $form['domain_alias_new'][$index]['defaults'] = array(
      '#type' => 'checkboxes', '#required' => FALSE, '#options' => $languages_options, '#default_value' => 0
    );
  }
  
  $form['#submit'][] = 'domain_languages_domain_alias_form_submit';
  $form['#validate'] = array(
    'domain_languages_domain_alias_form_validate' 
  );
}

/**
 * FAPI for domain_alias_form()
 *
 * Overrides domain_alias validate function to add languages field check (and void "No changes were made." error if only the languages are changed)
 */
function domain_languages_domain_alias_form_validate($form, &$form_state) {
  // Validate aliases
  $aliases = array();
  
  // Array that stores the number of default aliases assigned to each language (can't be more than one)
  $nb_default_aliases_by_lang = array();
      
  // Validate updates -- this array might not have data.
  if (isset($form_state['values']['domain_alias'])) {
    foreach ($form_state['values']['domain_alias'] as $count => $alias) {
      $validate = TRUE;
      // Delete requests and unchanged aliases do not need the validation step.
      $original_alias = domain_alias_lookup(NULL, $count);
      $original_alias['languages'] = domain_languages_get_domain_languages($original_alias['domain_id'], $original_alias['alias_id']);
      
      // Validating that the user didn't select a language as default that is not assigned to this domain
      foreach ($alias['defaults'] as $lang_code =>$value) {        
        // If language has been selected as default
        if (is_string($value) && $lang_code == $value) {
          if (!isset($nb_default_aliases_by_lang[$lang_code])) {
            $nb_default_aliases_by_lang[$lang_code] = 0;
          }         
          
          // Does the language has the main domain as default already?
          $default_alias_domain_for_language = domain_languages_get_default_domain_alias_for_language($form_state['values']['domain_id'], $lang_code);
          
          // If the main domain already is the default domain for this language, raise an error
          if (isset($default_alias_domain_for_language) && $default_alias_domain_for_language['type'] == 'domain') {            
            form_error($form['domain'], t("!alias_pattern can't be the default alias for !lang_code language as the !main_domain domain is already the default domain for it", 
                      array('!alias_pattern' => $alias['pattern'], '!lang_code' => $lang_code, '!main_domain' => $default_alias_domain_for_language['details']['subdomain'])));
            break;
          }          
          
          // Incrementing for current alias
          $nb_default_aliases_by_lang[$lang_code]++;
          
          // If language is not part of alias languages
          if(!is_string($alias['languages'][$lang_code]) || $alias['languages'][$lang_code] != $lang_code) {
            $validate = FALSE;
            form_error($form['domain'], t("You can't select a language as default if it's not one of the alias language"));
            break;
          }
        }
      }
      
      // Validating that the user didn't select more than one alias as default for a language
      foreach ($nb_default_aliases_by_lang as $lang_code => $nb_domain_aliases) {
        if ($nb_domain_aliases > 1) {
          form_error($form['domain'], t("You can't select an alias as default for a language if the language has already an alias as default alias"));
        }
      }
      
      if ($original_alias['pattern'] == $alias['pattern']) {
        // In this case, no change and no error set, unless no changes made.
        $validate = FALSE;
      }
      if ($original_alias['redirect'] != $alias['redirect']) {
        // In this case, we updated the redirect settings only.
        $validate = FALSE;
        $aliases[] = 'update placeholder';
      }
      if (sizeof($original_alias['languages']) != sizeof($alias['languages']) || array_diff($original_alias['languages'], $alias['languages'])) {
        // In this case, we updated the languages only.
        $validate = FALSE;
        $aliases[] = 'update placeholder';
      }
      if ($form_state['values']['domain_alias'][$count]['delete']) {
        // Set a value so we do not return an error on empty array.
        $aliases[] = 'delete placeholder';
        $validate = FALSE;
      }
      if ($validate) {
        // Run the validation routine.
        $aliases[] = _domain_alias_validate($form, $alias, $count, $aliases, 'domain_alias');
      }
    }
  }
  
  // Validate new domain aliases -- this array should always have data.
  foreach ($form_state['values']['domain_alias_new'] as $count => $alias) {
    if (empty($alias['pattern'])) {
      continue;
    }
    $aliases[] = _domain_alias_validate($form, $alias, $count, $aliases, 'domain_alias_new');
          
    // Validating that the user didn't select a language as default that is not assigned to this domain
    foreach ($alias['defaults'] as $lang_code =>$value) {
      // If language has been selected as default
      if (is_string($value) && $lang_code == $value) {
        // Does the language has the main domain as default already?
        $default_alias_domain_for_language = domain_languages_get_default_domain_alias_for_language($form_state['values']['domain_id'], $lang_code);
        
        // If the main domain already is the default domain for this language, raise an error
        if (isset($default_alias_domain_for_language) && $default_alias_domain_for_language['type'] == 'domain') {            
          form_error($form['domain'], t("!alias_pattern can't be the default alias for !lang_code language as the !main_domain domain is already the default domain for it", 
                    array('!alias_pattern' => $alias['pattern'], '!lang_code' => $lang_code, '!main_domain' => $default_alias_domain_for_language['details']['subdomain'])));
          break;
        }             
        
        if (!isset($nb_default_aliases_by_lang[$lang_code])) {
          $nb_default_aliases_by_lang[$lang_code] = 0;
        }
        $nb_default_aliases_by_lang[$lang_code]++;
        
        // If language is not part of alias languages
        if(!is_string($alias['languages'][$lang_code]) || $alias['languages'][$lang_code] != $lang_code) {
          $validate = FALSE;
          form_error($form['domain'], t("You can't select a language as default if it's not one of the alias language"));
          break;
        }
      }
    }
  
    // Validating that the user didn't select more than one alias as default for a language
    foreach ($nb_default_aliases_by_lang as $lang_code => $nb_domain_aliases) {
      if ($nb_domain_aliases > 1) {
        form_error($form['domain'], t("You can't select an alias as default for a language if the language has already an alias as default alias"));
      }
    }
  }
  if (empty($aliases)) {
    form_error($form['domain'], t('No changes were made.'));
  }
}

/**
 * Submit callback for domain form
 *
 * @param array $form The domain form
 * @param array $form_state The form values
 */
function domain_languages_domain_alias_form_submit($form, &$form_state) {
  // For existing domain aliases
  if (isset($form_state['values']['domain_alias'])) {
    foreach ($form_state['values']['domain_alias'] as $domain_alias) {
      if (isset($form_state['values']['domain_alias'])) {
        
        // Constructing languages and default informations from submitted values
        $languages_infos = array();
        foreach ($domain_alias['languages'] as $lang_code => $value) {
          if (is_array($value) && isset($value['code'])) {
            $languages_infos[] = $value;
          } 
          elseif (is_string($value) && $value == $lang_code) {  
            $languages_infos[] = array(
              'code' => $lang_code,
              'default' => (is_string($domain_alias['defaults'][$lang_code]) && $domain_alias['defaults'][$lang_code] == $lang_code),
            );
          }
        }
        
        // Removing previous information
        domain_languages_delete_languages_for_domain_alias($form_state['values']['domain_id'], $domain_alias['alias_id']);
        
        // Saving updated information
        domain_languages_save_languages($languages_infos, $form_state['values']['domain_id'], $domain_alias['alias_id']);
      }
    }
  }
  
  // For new domaon aliases
  if (isset($form_state['values']['domain_alias_new'])) {
    foreach ($form_state['values']['domain_alias_new'] as $domain_alias) {
      
      // Don't do anything if the pattern field is empty
      if (trim($domain_alias['pattern']) != '' && isset($domain_alias['languages'])) {
        // getting the previously saved data for this alias (to get the alias id)
        $domain_alias_lookedup = domain_alias_lookup($domain_alias['pattern']);
        
        // Don't save anything if the lookup failed (previous submit handler didn't save anything)
        if (isset($domain_alias_lookedup['alias_id'])) {
        
          $languages_infos = array();
          foreach ($domain_alias['languages'] as $lang_code => $value) {          
            if (is_array($value) && isset($value['code'])) {
              $languages_infos[] = $value;
            } 
            elseif (is_string($value) && $value == $lang_code) {  
              $languages_infos[] = array(
                'code' => $lang_code,
                'default' => (is_string($domain_alias['defaults'][$lang_code]) && $domain_alias['defaults'][$lang_code] == $lang_code),
              );
            }
          }
                  
          domain_languages_save_languages($languages_infos, $form_state['values']['domain_id'], $domain_alias_lookedup['alias_id']);
        }
      }
    }
  }
}

/**
 * Implementation of hook_theme_registry_alter()
 *
 * Used to override domain_alias_form theme to include the language column and field
 *
 * @param object $theme_registry The themes registry
 */
function domain_languages_theme_registry_alter(&$theme_registry) {
  $theme_registry['domain_alias_form']['file'] = 'domain_languages.themes.inc';
  $theme_registry['domain_alias_form']['function'] = 'theme_domain_alias_languages_form';
  $theme_registry['domain_alias_form']['include files'] = array(
    drupal_get_path('module', 'domain_languages') . '/domain_languages.themes.inc' 
  );
  $theme_registry['domain_alias_form']['theme paths'] = array(
    drupal_get_path('module', 'domain_languages') 
  );
  $theme_registry['domain_alias_form']['theme path'] = drupal_get_path('module', 'domain_languages');
}

/**
 * Gets the language associated to a domain
 *
 * @param int $domain_id
 * @param int $alias_id (optional) the alias id to search for
 * @param boolean $reset (optional) Whether we want to reset the cache
 */
function domain_languages_get_domain_languages($domain_id, $alias_id = NULL, $reset = FALSE) {
  static $domain_languages_matches = array();
  
  // Reset cache if requested
  if ($reset !== FALSE) {
    $domain_languages_matches = array();
  }
  
  // Cache key
  $cache_key = $domain_id;
  if (isset($alias_id)) {
    $cache_key .= "-" . $alias_id;
  }
  
  // If we already have the result in cache, return the cache value
  if (isset($domain_languages_matches[$cache_key])) {
    return $domain_languages_matches[$cache_key];
  }
  
  $domain_language = array();
  
  // get the language code from the database
  if (isset($alias_id)) {
    $domain_language_result = db_query("SELECT language FROM {domain_languages} WHERE domain_id = %d AND alias_id = %d", $domain_id, $alias_id);
  }
  else {
    $domain_language_result = db_query("SELECT language FROM {domain_languages} WHERE domain_id = %d AND alias_id = %d", $domain_id, DOMAIN_LANGUAGES_MAIN_DOMAIN_ALIAS_ID);
  }
  
  if ($domain_language_result !== FALSE) {
    while ($domain_language_data = db_fetch_object($domain_language_result)) {
      $domain_language[] = $domain_language_data->language;
    }
  }
  
  // Caching the result
  $domain_languages_matches[$cache_key] = $domain_language;
  
  return $domain_language;
}

/**
 * Gets the language and default parameters associated to a domain
 *
 * @param int $domain_id
 * @param int $alias_id (optional) the alias id to search for
 * @param boolean $reset (optional) Whether we want to reset the cache
 */
function domain_languages_get_domain_languages_defaults($domain_id, $alias_id = NULL, $reset = FALSE) {
  static $domain_languages_defaults_matches = array();
  
  // Reset cache if requested
  if ($reset !== FALSE) {
    $domain_languages_defaults_matches = array();
  }
  
  // Cache key
  $cache_key = $domain_id;
  if (isset($alias_id)) {
    $cache_key .= "-" . $alias_id;
  }
  
  // If we already have the result in cache, return the cache value
  if (isset($domain_languages_defaults_matches[$cache_key])) {
    return $domain_languages_defaults_matches[$cache_key];
  }
  
  $domain_language = array();
  
  // get the language code from the database
  if (isset($alias_id)) {
    $domain_language_result = db_query("SELECT language, `default` FROM {domain_languages} WHERE domain_id = %d AND alias_id = %d", $domain_id, $alias_id);
  }
  else {
    $domain_language_result = db_query("SELECT language, `default` FROM {domain_languages} WHERE domain_id = %d AND alias_id = %d", $domain_id, DOMAIN_LANGUAGES_MAIN_DOMAIN_ALIAS_ID);
  }
  
  if ($domain_language_result !== FALSE) {
    while ($domain_language_data = db_fetch_object($domain_language_result)) {      
      $domain_language[$domain_language_data->language] = array();
      $domain_language[$domain_language_data->language]['code'] = $domain_language_data->language;
      $domain_language[$domain_language_data->language]['default'] = $domain_language_data->default;
    }
  }
  
  // Caching the result
  $domain_languages_defaults_matches[$cache_key] = $domain_language;
  
  return $domain_language;
}

/**
 * Return whether the passed domain/alias has the passed language assigned to it.
 * 
 * @param 
 * 	 $language The language to check for
 * @param 
 *   $domain_id The domain id to check
 * @param 
 *   $domain_alias_id (optional) The alias id to check
 */
function domain_languages_domain_has_language($language, $domain_id, $domain_alias_id = -1) {
  $domain_language_query = db_query("SELECT COUNT(*) as found_element_nb FROM {domain_languages} WHERE domain_id = %d AND alias_id = %d AND language = '%s'", $domain_id, $domain_alias_id, $language);
  $found_element = db_fetch_object($domain_language_query);
  
  if ($found_element->found_element_nb > 0) {
    return TRUE;
  }
  
  return FALSE;  
}

/**
 * Gets the aliases or domains associated to a language/country
 *
 * @param int $main_domain_id main_domain id
 * @param string $language the language code for which we are looking the domain or alias
 * @param array $excluded_aliases List of pattern to exclude
 * @param boolean $reset (optional) Whether we want to reset the cache
 *
 * @return array The found domains and aliases (domain_id, subdomain and alias_id) for the language. May return NULL if no domain or alias is set for the passed language.
 */
function domain_languages_get_domain_alias_for_language($main_domain_id, $language, $excluded_aliases = NULL, $reset = FALSE) {
  static $domain_languages_domain_aliases_language = array();
  
  // Reset the cache if requested
  if ($reset !== FALSE) {
    $domain_languages_domain_aliases_language = array();
  }
  
  // Cache key
  $cache_key = $main_domain_id . '-' . $language;
  
  // If we already have the result in cache, return the cache value
  if (isset($domain_languages_domain_aliases_language[$cache_key])) {
    return $domain_languages_domain_aliases_language[$cache_key];
  }
  
  $domains_aliases = NULL;
  
  // get the domain or alias from the database
  $domains_aliases_query = "SELECT dl.domain_id, d.subdomain, dl.alias_id, da.pattern FROM {domain_languages} AS dl " . "INNER JOIN {domain} AS d ON d.domain_id = dl.domain_id ";
  
  $domain_alias_left_join = "LEFT JOIN {domain_alias} AS da ON da.alias_id = dl.alias_id";
  
  // If excluded aliases are passed, add the conditions
  if (isset($excluded_aliases)) {
    foreach ($excluded_aliases as $excluded_alias_pattern) {
      $domain_alias_left_join .= " AND da.pattern NOT LIKE '" . _domain_languages_placeholders_to_sql($excluded_alias_pattern) . "'";
    }
  }
  
  $domains_aliases_query .= $domain_alias_left_join . " WHERE d.domain_id = %d AND dl.language = '%s'" . " ORDER BY dl.alias_id DESC";
  
  $domains_aliases_result = db_query($domains_aliases_query, $main_domain_id, $language);
  
  if ($domains_aliases_result) {
    $domains_aliases = array();
    
    while ($domain_alias_data = db_fetch_object($domains_aliases_result)) {
      // Add the current result to the result array
      $domains_aliases[] = $domain_alias_data;
    }
  }
  
  // Caching the result
  $domain_languages_domain_aliases_language[$cache_key] = $domains_aliases;
  
  return $domains_aliases;
}

/**
 * Gets the default domain or alias for a given language code.
 * 
 * @param 
 *   $main_domain_id Main domain id
 * @param 
 *   $language Language to look the default domain or alias for
 * 
 * @return 
 *   array The domain or alias details ('type' => 'domain' or 'alias', 'details' => result of domain_lookup() or domain_alias_lookup())
 */
function domain_languages_get_default_domain_alias_for_language($main_domain_id, $language) {
  $default_domain_alias = NULL;
  
  // Checking if the column 'default' of domain_languages already exists or not
  if (db_column_exists('domain_languages', 'default')) {    
    $default_domain_alias_query = db_query("SELECT domain_id, alias_id FROM {domain_languages} WHERE domain_id = %d AND language = '%s' AND `default` = 1 ", $main_domain_id, $language);
    $default_domain_alias_id = db_fetch_object($default_domain_alias_query);
    
    $default_domain_alias = array();
    if ($default_domain_alias_id->alias_id != DOMAIN_LANGUAGES_MAIN_DOMAIN_ALIAS_ID) {
      $default_domain_alias['type'] = 'alias';
      $default_domain_alias['details'] = domain_alias_lookup(NULL, $default_domain_alias_id->alias_id);
    }
    else {
      $default_domain_alias['type'] = 'domain';
      $default_domain_alias['details'] = domain_lookup($default_domain_alias_id->domain_id);
    }
  }
  
  return $default_domain_alias;
}

/**
 * Deletes languages information for all aliases of a domain or a specific alias
 * 
 * @param 
 *   $domain_id Domain identifier for which we want to delete the languages
 * @param 
 * 	 $alias_id (optional) Alias identifier for which we want to delete the languages
 */
function domain_languages_delete_languages_for_domain_alias($domain_id, $alias_id = NULL) {
  if (isset($alias_id)) {
    db_query("DELETE FROM {domain_languages} WHERE domain_id = %d AND alias_id = %d", $domain_id, $alias_id);
  }
  else {
    db_query("DELETE FROM {domain_languages} WHERE domain_id = %d", $domain_id);
  }
}

/**
 * Function that saves languages for a given domain or alias
 *
 * @param array $languages Languages to add
 * @param int $domain_id Domain to add the language to
 * @param int $alias_id Alias to add the language to (Optional)
 *
 * @return boolean FALSE if the save didn't perform successfully
 */
function domain_languages_save_languages($languages, $domain_id, $alias_id = DOMAIN_LANGUAGES_MAIN_DOMAIN_ALIAS_ID) {  
  foreach ($languages as $language_detail) {    
    // Checking if the column 'default' of domain_languages doesn't exists, use old queries without the column
    if (!db_column_exists('domain_languages', 'default')) {    
      // Gets the language code the new or old way depending if the domain_settings.php content is up to date
      $language = (is_array($language_detail) && isset($language_detail['code']) ? $language_detail['code'] : $language_detail);
      
      $language_exists_result = db_query("SELECT COUNT(*) as language_count FROM {domain_languages} WHERE domain_id = %d AND alias_id = %d AND language = '%s'", $domain_id, $alias_id, $language);
      $language_exists = db_fetch_object($language_exists_result);
      
      if (!$language_exists || $language_exists->language_count <= 0) {
        $result = db_query("INSERT INTO {domain_languages} (domain_id, language, alias_id) VALUES (%d, '%s', %d, %d)", $domain_id, $language, $alias_id);
      }
    }
    // If the default column exists, use new code
    else {    
      $language = $language_detail['code'];    
      $is_default_domain_alias = (isset($language_detail['default']) && $language_detail['default'] ? 1 : 0);
      
      $language_exists_result = db_query("SELECT COUNT(*) as language_count FROM {domain_languages} WHERE domain_id = %d AND alias_id = %d AND language = '%s'", $domain_id, $alias_id, $language);
      $language_exists = db_fetch_object($language_exists_result);
      
      if ($language_exists && $language_exists->language_count > 0) {
        $result = db_query("UPDATE {domain_languages} SET `default` = %d WHERE domain_id = %d AND alias_id = %d AND language = 'en-WW'", $is_default_domain_alias, $domain_id, $alias_id, $language);
      }
      else {
        $result = db_query("INSERT INTO {domain_languages} (domain_id, language, `default`, alias_id) VALUES (%d, '%s', %d, %d)", $domain_id, $language, $is_default_domain_alias, $alias_id);
      }
    }
  }
  
  return $result;
}

/**
 * Replace placeholders * with SQL placeholders %
 *
 * @param $subdomain
 * String to work on.
 * @return
 * String with replaced values.
 */
function _domain_languages_placeholders_to_sql($subdomain, $reverse = FALSE) {
  $placeholders = array(
    '*' => '%' 
  );
  if ($reverse) {
    return str_replace($placeholders, array_keys($placeholders), $subdomain);
  }
  else {
    return str_replace(array_keys($placeholders), $placeholders, $subdomain);
  }
}