<?php
// $Id: domain_languages.themes.inc,v 1.0 2011/01/17 11:50:58 bpresles Exp $

/**
 * @defgroup domain_languages Domain Languages: Domain language support
 *
 * Allows domain to be associated to a language
 */

/**
 * @file
 * Domain languages themes
 *
 * @ingroup domain_languages
 */

/**
 * Domain Alias Form theming. Overrides the domain_alias module one's.
 */
function theme_domain_alias_languages_form($form) {
  $output = '';
  $redirect = t('Check the redirect box to send requests for an alias to the registered domain.');
  $output .= drupal_render($form['domain_help']);
  $output .= '<br /><h3>'. drupal_render($form['domain']) .'</h3>';
  // Edit existing records.
  $elements = element_children($form['domain_alias']);
  if (!empty($elements)) {
    $header = array(t('Id'), t('Redirect'), t('Pattern'), t('Languages'), t('Default alias for languages'), t('Delete'));
    $rows = array();
    foreach ($elements as $element) {
      $rows[] = array(
        $form['domain_alias'][$element]['alias_id']['#value'],
        drupal_render($form['domain_alias'][$element]['redirect']),
        drupal_render($form['domain_alias'][$element]['pattern']),
        drupal_render($form['domain_alias'][$element]['languages']),
        drupal_render($form['domain_alias'][$element]['defaults']),
        drupal_render($form['domain_alias'][$element]['delete']),
      );
    }
    $output .= theme('table', $header, $rows);
    $output .= '<p><em>'. $redirect .'</em></p>';
  }
  else {
    $output .= '<p>'. t('There are no aliases recorded for this domain.') .'</p>';
  }
  // Add new records.
  $output .= '<br /><h3>'. drupal_render($form['domain_new']) .'</h3>';
  $output .= '<p>'. drupal_render($form['domain_new_help']) .'</p>';
  $header = array(t('Redirect'), t('Pattern'), t('Languages'), t('Default alias for languages'));
  $rows = array();
  foreach (element_children($form['domain_alias_new']) as $element) {
    $rows[] = array(
      drupal_render($form['domain_alias_new'][$element]['redirect']),
      drupal_render($form['domain_alias_new'][$element]['pattern']),
      drupal_render($form['domain_alias_new'][$element]['languages']),
      drupal_render($form['domain_alias_new'][$element]['defaults']),
    );
  }
  $output .= theme('table', $header, $rows);
  $output .= '<p><em>'. $redirect .'</em></p>';
  $output .= drupal_render($form);
  return $output;
}